/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@push.rocks/smarturl',
  version: '3.0.7',
  description: 'a url parsing lib'
}
